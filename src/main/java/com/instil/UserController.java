/*
 * Copyright 2017 Instil Software.
 */
package com.instil;

import com.instil.security.AuthenticatedUser;
import com.instil.security.AuthenticationFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserController {

    @Autowired
    AuthenticationFacade authenticationFacade;

    @RequestMapping("/users")
    public ResponseEntity<String> getUsers() {
        AuthenticatedUser user = authenticationFacade.authentication();
        log.debug(String.format("'%s' is making this request.", user.getName()));
        return ResponseEntity.ok("All the users");
    }

}
