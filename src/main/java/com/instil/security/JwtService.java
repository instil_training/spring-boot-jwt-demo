package com.instil.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class JwtService {

    @Autowired
    private JwtConfiguration jwtConfiguration;

    private String prefix = "Bearer ";

    public String createJwtFor(String username) {
        Claims claims  = new DefaultClaims();
        claims.setSubject(username);
        claims.setExpiration(new Date(System.currentTimeMillis() + jwtConfiguration.getExpiration()));
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, jwtConfiguration.getSecret())
                .compact();
        return prefix + token;
    }

    public Authentication authenticate(String tokenHeader) {
        if (tokenHeader == null) {
            return null; // urgh ... again!
        }
        String token = tokenHeader.substring(prefix.length(), tokenHeader.length());
        if (token != null) {
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtConfiguration.getSecret())
                    .parseClaimsJws(token)
                    .getBody();
            Date expiry = claims.getExpiration();
            String username = claims.getSubject();
            if (expiry.after(new Date()) && username != null) {
                return new AuthenticatedUser(username);
            }
        }
        return null;
    }

}
