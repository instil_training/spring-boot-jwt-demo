package com.instil.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("jwt")
public class JwtConfiguration {

    private long expiration;
    private String secret;
    private String header;

}
