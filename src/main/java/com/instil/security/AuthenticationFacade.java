package com.instil.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFacade {

    public AuthenticatedUser authentication() {
        return (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication();
    }

}
