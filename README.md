## Using JWT in your Spring Boot application.

The code in this project is based heavily on the post by Dylan Meeus of Auth0 which can be found [here](https://auth0.com/blog/securing-spring-boot-with-jwts/).

This is not a complete solution but should be used as the basis for your own code. 
Specifically, the authentication provided by the `DemoAuthenticationProvider` class is inadequate for a real project, 
but is there to show where you would plugin your own authentication manager. 
Also, the verification of the JWT token in the `JWTService` class could be improved quite a bit.

